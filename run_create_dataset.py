import os
import wget
import logging
import argparse
from zipfile import ZipFile

from src.data.preprocessing.data_handler import DataHandler
from dotenv import find_dotenv, load_dotenv
from src.common.logger import setup_logging
from src.common.timing import log_time_taken
from src.common.config_parser import ConfigParser
from src.utils.path_handler import PathHandler

logger = logging.getLogger(__loader__.name)


def download_raw_data_to_disk(url: str, raw_data_dir: str) -> None:
    """Loads the url from the env var and downloads raw data to disk"""
    if not os.path.exists(f'{raw_data_dir}/{url.split("/")[-1]}'):
        wget.download(url, raw_data_dir)
        # opening the zip file in READ mode
        with ZipFile(f'{raw_data_dir}/{url.split("/")[-1]}', "r") as zip:
            # extracting all the files
            zip.extractall(path=raw_data_dir)


def main(args):
    """
    Downloads and Preprocess the raw data
    """
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    logging.info(f"Loading Configurations")
    # create an instance of configuration file
    config = ConfigParser.from_yaml(args.config).config

    PathHandler(
        [
            config.get("paths").get("raw_data_path"),
            config.get("paths").get("processed_data_path"),
        ]
    ).create_paths_if_not_exists()

    logging.info(f"Downloading and unzipping raw data to disk")
    download_raw_data_to_disk(
        url=os.getenv("DATA_URL"), raw_data_dir=config.get("paths").get("raw_data_path")
    )

    logging.info(
        f"Splitting data to train-test-validation sets and making pairs of titles and images "
        f"for image and text encoders"
    )
    handler = DataHandler(
        raw_data_path=f'{config.get("paths").get("raw_data_path")}/dataset',
        processed_data_path=config.get("paths").get("processed_data_path"),
    )
    handler.split_data(
        max_train_utterances_per_cluster=5,
        train_percentage=0.8,
        validation_percentage=0.2,
    )
    handler.make_pairs(save_to_csv=True)
    logging.info(f"Data Preparation Finished")


if __name__ == "__main__":
    # Initiate the parser
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", type=str, help="path to yaml file")

    # Read arguments from the command line
    args = parser.parse_args()

    setup_logging()
    with log_time_taken(logger):
        main(args)
