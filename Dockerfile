FROM python:3.8.6-buster

WORKDIR /Dedub

ADD . /Dedub

RUN pip install -r requirements.txt