import os
import logging
import argparse
from dotenv import find_dotenv, load_dotenv

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

from src.common.logger import setup_logging
from src.common.timing import log_time_taken
from src.common.config_parser import ConfigParser
from src.data.data_orchestrator_inference import DataOrchestratorInference
from src.models.inference import DedubInference
from src.utils.set_logging_level import LoggingLevel


logger = logging.getLogger(__loader__.name)


def main(args):
    """Interface for inference"""

    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    logging.info(f"Defining modalities")
    if args.first_image and args.second_image:
        network_modalities = (
            "all" if args.first_title and args.second_title else "image"
        )
    elif args.first_title and args.second_title:
        network_modalities = "text"
    else:
        logging.error("Please define the proper input pairs")
        logging.shutdown()

    logging.info(f"Inference will use '{network_modalities.upper()}' modalities")
    """
    python run_inference.py
        --config src/config/config.py
        --first_title "1 Million People Are Using This App to Learn a New Language in Just 3 Weeks -- try it now for free!"
        --second_title "22 Hilarious Dog Posts From Snapchat"
        --first_image data/raw/dataset/images/1_5_7120530738946192667.jpg
        --second_image data/raw/dataset/images/1_6_-3673414655321898241.jpg
    """

    logging.info(f"Loading Configurations")
    # create an instance of configuration file
    config = ConfigParser.from_yaml(args.config).config
    (
        image_train_params,
        paths,
        image_encoder,
        text_train_params,
        title_encoder,
        path_to_model,
        transformers_logging_level,
    ) = (
        config.get("train_params").get("image_dataset"),
        config.get("paths"),
        config.get("image_encoder"),
        config.get("train_params").get("text_dataset"),
        config.get("title_encoder"),
        config.get("inference").get("path_to_model"),
        config.get("transformers_logging").get("level"),
    )

    # set logging level for transformers
    LoggingLevel(
        library_to_supress="transformers",
        level=transformers_logging_level,
    )()

    logging.info(f"Loading Inputs Pairs for inference")
    data_orchestrator_inference = DataOrchestratorInference(
        args=args,
        network_modalities=network_modalities,
        image_encoder=image_encoder,
        title_encoder=title_encoder,
    )

    data_orchestrator_inference.prepare_inputs_for_inference()

    logging.info(f"Initialize Data Generators")

    logging.info(
        f"Initialize Model with modalities: {config.get('train_params').get('network_modalities')}"
    )
    logging.info("Start Prediction")
    prediction, probability = DedubInference(
        inputs_for_inference=data_orchestrator_inference.inputs_for_inference,
        path_to_trained_model=path_to_model,
        paths=paths,
        title_encoder=title_encoder,
        image_encoder=image_encoder,
        network_modalities=network_modalities,
    ).inference()

    logging.info(f"Pairs are {prediction} with probability {probability:.2f}%")


if __name__ == "__main__":
    # Initiate the parser
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", type=str, help="path to yaml file")
    parser.add_argument("--first_title", type=str, help="first title", default=None)
    parser.add_argument("--second_title", type=str, help="second title", default=None)
    parser.add_argument(
        "--first_image", type=str, help="path to first image", default=None
    )
    parser.add_argument(
        "--second_image", type=str, help="path to second image", default=None
    )

    # Read arguments from the command line
    args = parser.parse_args()

    setup_logging()
    with log_time_taken(logger):
        main(args)
