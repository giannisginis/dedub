from src.tests.test_main import image_encoder, title_encoder, InputsDataClass
from src.data.data_orchestrator_inference import DataOrchestratorInference
from src.data.generators.text_data_generators import BertSemanticDataGenerator
from src.data.generators.image_data_generator import ImageDataGenerator


data_orchestrator_inf = DataOrchestratorInference(
    image_encoder=image_encoder,
    title_encoder=title_encoder,
    network_modalities="",
    args=InputsDataClass(),
)


def test_data_orchestrator_inference_text_modalities():
    """Validates if text data loader is initialised for text modality"""
    data_orchestrator_inf.network_modalities = "text"
    data_orchestrator_inf.prepare_inputs_for_inference()

    assert not isinstance(
        data_orchestrator_inf.inputs_for_inference, ImageDataGenerator
    )
    assert isinstance(
        data_orchestrator_inf.inputs_for_inference, BertSemanticDataGenerator
    )


def test_data_orchestrator_inference_all_modalities():
    """Validates if image data loader is initialised for all modalities"""
    data_orchestrator_inf.network_modalities = "all"
    data_orchestrator_inf.prepare_inputs_for_inference()

    assert isinstance(data_orchestrator_inf.inputs_for_inference, ImageDataGenerator)
    assert not isinstance(
        data_orchestrator_inf.inputs_for_inference, BertSemanticDataGenerator
    )


def test_data_orchestrator_inference_image_modalities():
    """Validates if text data loader is initialised for image modality"""
    data_orchestrator_inf.network_modalities = "image"
    data_orchestrator_inf.prepare_inputs_for_inference()

    assert isinstance(data_orchestrator_inf.inputs_for_inference, ImageDataGenerator)
    assert not isinstance(
        data_orchestrator_inf.inputs_for_inference, BertSemanticDataGenerator
    )
