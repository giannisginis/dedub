import transformers
import numpy as np
import tensorflow as tf
from typing import List, Union, Tuple


class BertSemanticDataGenerator(tf.keras.utils.Sequence):
    """Generates batches of data.

    Args:
        sentence_pairs: Array of premise and hypothesis input sentences.
        labels: Array of labels.
        batch_size: Integer batch size.
        shuffle: boolean, whether to shuffle the data.
        include_targets: boolean, whether to incude the labels.

    Returns:
        Tuples `([input_ids, attention_mask, `token_type_ids], labels)`
        (or just `[input_ids, attention_mask, `token_type_ids]`
         if `include_targets=False`)
    """

    def __init__(
        self,
        sentence_pairs: np.array,
        labels: np.array = None,
        batch_size: int = 1,
        model_max_length: int = 128,
        shuffle: bool = True,
        include_targets: bool = True,
        do_lower_case: bool = True,
        add_special_tokens: bool = True,
        return_attention_mask: bool = True,
        return_token_type_ids: bool = True,
        padding: str = "max_length",
        return_tensors: str = "tf",
    ):
        self.sentence_pairs = sentence_pairs
        self.labels = labels
        self.shuffle = shuffle
        self.batch_size = batch_size
        self.include_targets = include_targets
        self.do_lower_case = do_lower_case
        self.model_max_length = model_max_length
        # Load our BERT Tokenizer to encode the text.
        # bert-base-uncased pretrained model.
        self.tokenizer = transformers.BertTokenizer.from_pretrained(
            "bert-base-uncased",
            do_lower_case=self.do_lower_case,
            model_max_lenght=self.model_max_length,
        )
        # initialize parameters for tokenizer
        self.add_special_tokens = add_special_tokens
        self.return_attention_mask = return_attention_mask
        self.return_token_type_ids = return_token_type_ids
        self.return_tensors = return_tensors
        self.padding = padding

        self.indexes = np.arange(len(self.sentence_pairs))
        self.on_epoch_end()

    def __len__(self):
        """Denotes the number of batches per epoch"""
        return len(self.sentence_pairs) // self.batch_size

    def __getitem__(
        self, idx
    ) -> Tuple[List[Union[np.ndarray, List[np.ndarray], None]], np.ndarray] or List[
        Union[np.ndarray, List[np.ndarray], None]
    ]:
        """Batch data generator"""

        # Retrieves the batch of index.
        indexes = self.indexes[idx * self.batch_size : (idx + 1) * self.batch_size]
        sentence_pairs = self.sentence_pairs[indexes]

        # With BERT tokenizer's batch_encode_plus batch of both the sentences are
        # encoded together and separated by [SEP] token.
        encoded = self.tokenizer.batch_encode_plus(
            sentence_pairs.tolist(),
            add_special_tokens=True,
            max_length=self.model_max_length,
            return_attention_mask=True,
            return_token_type_ids=True,
            padding="max_length",
            return_tensors="tf",
        )

        # Convert batch of encoded features to numpy array.
        input_ids = np.array(encoded["input_ids"], dtype="int32")
        attention_masks = np.array(encoded["attention_mask"], dtype="int32")
        token_type_ids = np.array(encoded["token_type_ids"], dtype="int32")

        # Set to true if data generator is used for training/validation.
        if self.include_targets:
            labels = np.array(self.labels[indexes], dtype="int32")
            return [input_ids, attention_masks, token_type_ids], labels
        else:
            return [input_ids, attention_masks, token_type_ids]

    def on_epoch_end(self) -> None:
        """On the end of each epoch shuffles data"""
        # Shuffle indexes after each epoch if shuffle is set to True.
        if self.shuffle:
            np.random.RandomState(42).shuffle(self.indexes)
