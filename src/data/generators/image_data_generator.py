import numpy as np
import tensorflow as tf
from typing import Tuple, List, Union
import transformers


class ImageDataGenerator(tf.keras.utils.Sequence):
    """Generates batches of data.

    Args:
        image_pairs: Array of premise and hypothesis input sentences.
        labels: Array of labels.
        batch_size: Integer batch size.
        shuffle: boolean, whether to shuffle the data.
        include_targets: boolean, whether to include the labels.

    Returns:
        Tuples `[np.array(ancors), np.array(pairs),sentence_pairs_data_generator,], labels`
        or `[np.array(ancors), np.array(pairs),], labels` if `use_text_encoder=False`
        or `[np.array(ancors), np.array(pairs),sentence_pairs_data_generator,],` if `include_targets=False`
        or `[np.array(ancors), np.array(pairs),], labels` if `include_targets=False and use_text_encoder=False`
    """

    def __init__(
        self,
        image_pairs: np.array,
        labels: np.array = None,
        batch_size: int = 1,
        image_size: Tuple = (224, 224, 3),
        shuffle: bool = True,
        include_targets: bool = True,
        sentence_pairs=None,
        use_text_encoder: bool = False,
        do_lower_case: bool = True,
        model_max_length: int = None,
    ):
        self.image_pairs_pairs = image_pairs
        self.labels = labels
        self.shuffle = shuffle
        self.batch_size = batch_size
        self.include_targets = include_targets
        self.image_size = image_size
        self.sentence_pairs = sentence_pairs
        self.use_text_encoder = use_text_encoder
        self.model_max_length = model_max_length
        self.dataset = None

        if self.use_text_encoder:
            self.tokenizer = transformers.BertTokenizer.from_pretrained(
                "bert-base-uncased",
                do_lower_case=do_lower_case,
                model_max_lenght=model_max_length,
            )

        self.indexes = np.arange(len(self.image_pairs_pairs))
        self.on_epoch_end()

    def __len__(self) -> int:
        # Denotes the number of batches per epoch.
        return len(self.image_pairs_pairs) // self.batch_size

    def __getitem__(
        self, idx
    ) -> Tuple[List[Union[np.ndarray, List[np.ndarray], None]], np.ndarray] or List[
        Union[np.ndarray, List[np.ndarray], None]
    ]:
        """Batch data generator"""

        sentence_pairs_data_generator = None
        # Retrieves the batch of index.
        indexes = self.indexes[idx * self.batch_size : (idx + 1) * self.batch_size]
        batch_image_pairs = self.image_pairs_pairs[indexes]

        ancors = []
        pairs = []

        for image_pair_index in range(len(batch_image_pairs)):
            # add a matching example
            ancor_image = self.process_input(batch_image_pairs[image_pair_index][0])
            pair_image = self.process_input(batch_image_pairs[image_pair_index][1])

            ancors.append(tf.convert_to_tensor(ancor_image, dtype=tf.float32))
            pairs.append(tf.convert_to_tensor(pair_image, dtype=tf.float32))

        if self.use_text_encoder:
            sentence_pairs_data_generator = self.text_dataset(indexes=indexes)

        # Set to true if data generator is used for training/validation.
        if self.include_targets:
            labels = np.array(self.labels[indexes], dtype="float32")
            if self.use_text_encoder:
                return [
                    np.array(ancors),
                    np.array(pairs),
                    sentence_pairs_data_generator,
                ], labels
            else:
                return [np.array(ancors), np.array(pairs)], labels
        else:
            if self.use_text_encoder:
                return [
                    np.array(ancors),
                    np.array(pairs),
                    sentence_pairs_data_generator,
                ]
            else:
                return [np.array(ancors), np.array(pairs)]

    def on_epoch_end(self):
        # Shuffle indexes after each epoch if shuffle is set to True.
        if self.shuffle:
            np.random.RandomState(42).shuffle(self.indexes)

    def text_dataset(self, indexes) -> List[np.ndarray]:
        """Initializes bert based data loader if text modality is included"""
        sentence_pairs = self.sentence_pairs[indexes]

        # With BERT tokenizer's batch_encode_plus batch of both the sentences are
        # encoded together and separated by [SEP] token.
        encoded = self.tokenizer.batch_encode_plus(
            sentence_pairs.tolist(),
            add_special_tokens=True,
            max_length=self.model_max_length,
            return_attention_mask=True,
            return_token_type_ids=True,
            padding="max_length",
            return_tensors="tf",
        )

        # Convert batch of encoded features to numpy array.
        input_ids = np.array(encoded["input_ids"], dtype="int32")
        attention_masks = np.array(encoded["attention_mask"], dtype="int32")
        token_type_ids = np.array(encoded["token_type_ids"], dtype="int32")

        return [input_ids, attention_masks, token_type_ids]

    @staticmethod
    def decode_and_resize(img_path) -> np.ndarray:
        img = tf.io.read_file(img_path)
        img = tf.image.decode_jpeg(img, channels=3)
        img = tf.image.resize(img, (224, 224))
        img = tf.image.convert_image_dtype(img, tf.float32)
        return img

    def process_input(self, img_path) -> np.ndarray:
        return self.decode_and_resize(img_path)
