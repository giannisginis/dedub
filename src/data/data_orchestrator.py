import pandas as pd
from typing import Dict
import tensorflow as tf
from src.data.generators.image_data_generator import ImageDataGenerator
from src.data.generators.text_data_generators import BertSemanticDataGenerator


class DataOrchestrator:
    """Orchestrates the initialization and build of data generators for Training
    Args:
        image_train_params: Configuration for image train
        text_train_params: Configuration for text train
        network_modalities: Which modalities to use
        image_encoder: Parameters for image network
        title_encoder: Parameters for text network
        paths: A dictionary with the corresponding paths

    """

    def __init__(
        self,
        image_train_params: Dict,
        text_train_params: Dict,
        image_encoder: Dict,
        title_encoder: Dict,
        network_modalities: str,
        paths: Dict,
    ):
        self.image_train_params = image_train_params
        self.text_train_params = text_train_params
        self.image_encoder = image_encoder
        self.title_encoder = title_encoder
        self.network_modalities = network_modalities
        self.use_text_encoder = True if network_modalities == "all" else False
        self.paths = paths

        self.train_df_image, self.valid_df_image = None, None
        self.train_df_title, self.valid_df_title = None, None
        self.y_train_title, self.y_val_title = None, None
        self.__train_data, self.__valid_data = None, None

    @property
    def train_data(self):
        """Returns training data"""
        return self.__train_data

    @property
    def valid_data(self):
        """Returns validation data"""
        return self.__valid_data

    def load_datasets(self) -> None:
        """Loads data from disk"""
        self.train_df_image = pd.read_csv(
            f'{self.paths.get("processed_data_path")}/{self.image_train_params.get("train_set_filename")}'
        )
        self.valid_df_image = pd.read_csv(
            f'{self.paths.get("processed_data_path")}/{self.image_train_params.get("validation_set_filename")}'
        )

        self.train_df_title = pd.read_csv(
            f'{self.paths.get("processed_data_path")}/{self.text_train_params.get("train_set_filename")}'
        )
        self.valid_df_title = pd.read_csv(
            f'{self.paths.get("processed_data_path")}/{self.text_train_params.get("validation_set_filename")}'
        )

        if self.image_train_params.get("subset").get("use"):
            self.train_df_image = self.train_df_image.sample(frac=1).reset_index(
                drop=True
            )[: self.image_train_params.get("subset").get("train_set_proportion")]
            self.valid_df_image = self.valid_df_image.sample(frac=1).reset_index(
                drop=True
            )[: self.image_train_params.get("subset").get("train_set_proportion")]

        if self.text_train_params.get("subset").get("use"):
            self.train_df_title = self.train_df_title.sample(frac=1).reset_index(
                drop=True
            )[: self.text_train_params.get("subset").get("train_set_proportion")]
            self.valid_df_title = self.valid_df_title.sample(frac=1).reset_index(
                drop=True
            )[: self.text_train_params.get("subset").get("train_set_proportion")]

        self.y_train_title = tf.keras.utils.to_categorical(
            self.train_df_title.label,
            num_classes=self.title_encoder.get("output_dim"),
        )
        self.y_val_title = tf.keras.utils.to_categorical(
            self.valid_df_title.label,
            num_classes=self.title_encoder.get("output_dim"),
        )

    def build_data_generators(self) -> None:
        """Initializes data generators based on the chosen modalities"""
        if self.network_modalities == "image" or self.network_modalities == "all":
            self.__train_data = ImageDataGenerator(
                image_pairs=self.train_df_image[
                    ["ancor_image", "pair_image"]
                ].values.astype("str"),
                labels=self.train_df_image[["label"]].values,
                image_size=tuple(self.image_encoder.get("image_shape")),
                batch_size=self.image_encoder.get("batch_size"),
                shuffle=True,
                sentence_pairs=self.train_df_title[
                    ["first_title", "pair_title"]
                ].values.astype("str"),
                use_text_encoder=self.use_text_encoder,
                model_max_length=self.title_encoder.get("max_length"),
            )

            self.__valid_data = ImageDataGenerator(
                image_pairs=self.valid_df_image[
                    ["ancor_image", "pair_image"]
                ].values.astype("str"),
                labels=self.valid_df_image[["label"]].values,
                image_size=tuple(self.image_encoder.get("image_shape")),
                batch_size=self.image_encoder.get("batch_size"),
                shuffle=False,
                sentence_pairs=self.train_df_title[
                    ["first_title", "pair_title"]
                ].values.astype("str"),
                use_text_encoder=self.use_text_encoder,
                model_max_length=self.title_encoder.get("max_length"),
            )
        elif self.network_modalities == "text":
            self.__train_data = BertSemanticDataGenerator(
                self.train_df_title[["first_title", "pair_title"]].values.astype("str"),
                self.y_train_title,
                model_max_length=self.title_encoder.get("max_length"),
                batch_size=self.title_encoder.get("batch_size"),
                shuffle=True,
            )
            self.__valid_data = BertSemanticDataGenerator(
                self.valid_df_title[["first_title", "pair_title"]].values.astype("str"),
                self.y_val_title,
                model_max_length=self.title_encoder.get("max_length"),
                batch_size=self.title_encoder.get("batch_size"),
                shuffle=False,
            )
