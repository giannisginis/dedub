import pandas as pd
import argparse
from typing import Dict
from src.data.generators.text_data_generators import BertSemanticDataGenerator
from src.data.generators.image_data_generator import ImageDataGenerator


class DataOrchestratorInference:
    """Orchestrates the initialization and build of data generators for inference
    Args:
        args: Argument parser with input titles and images
        network_modalities: Which modalities to use
        image_encoder: Parameters for image network
        title_encoder: Parameters for text network

    """

    def __init__(
        self,
        args: argparse,
        network_modalities: str,
        image_encoder: Dict,
        title_encoder: Dict,
    ):
        title_1 = args.first_title
        title_2 = args.second_title
        image_1 = args.first_image
        image_2 = args.second_image

        self.image_encoder = image_encoder
        self.title_encoder = title_encoder
        self.input_titles = pd.DataFrame([[title_1, title_2]]).values.astype("str")
        self.input_images = pd.DataFrame([[image_1, image_2]]).values.astype("str")
        self.network_modalities = network_modalities
        self.use_text_encoder = True if self.network_modalities == "all" else False
        self.__inputs_for_inference = None

    @property
    def inputs_for_inference(self):
        """Returns the data for inference"""
        return self.__inputs_for_inference

    def prepare_inputs_for_inference(self) -> None:
        """Initializes data generators based on the chosen modalities"""
        if self.network_modalities == "text":
            self.__inputs_for_inference = BertSemanticDataGenerator(
                self.input_titles,
                model_max_length=128,
                batch_size=1,
                shuffle=False,
                include_targets=False,
            )
        else:
            self.__inputs_for_inference = ImageDataGenerator(
                image_pairs=self.input_images,
                image_size=self.image_encoder.get("image_shape"),
                labels=None,
                batch_size=1,
                shuffle=False,
                sentence_pairs=self.input_titles,
                use_text_encoder=self.use_text_encoder,
                model_max_length=self.title_encoder.get("max_length"),
                include_targets=False,
            )
