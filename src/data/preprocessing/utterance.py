# -*- coding: utf-8 -*-
from typing import Tuple, Optional, List, Any


class Utterance:
    """A representation of each utterance
    Args:
        counter: Line counter,
        title: A string title,
        img_name: Image name,
        cluster: Cluster label,
    """

    __all_instances = []

    def __init__(
        self,
        counter: int = None,
        title: str = None,
        img_name: str = None,
        cluster: int = None,
    ):
        self.__counter = counter
        self.__title = title
        self.__img_name = img_name
        self.__cluster = cluster

        self.__all_instances.append(self)

    def __repr__(self) -> str:
        return (
            f"Utterance({self.counter}, {self.title}, {self.img_name}, {self.cluster})"
        )

    @property
    def counter(self) -> int:
        """Returns counter"""
        return self.__counter

    @property
    def title(self) -> str:
        """Returns counter"""
        return self.__title

    @property
    def img_name(self) -> str:
        """Returns counter"""
        return self.__img_name

    @property
    def cluster(self) -> int:
        """Returns cluster label"""
        return self.__cluster

    @property
    def all_instances(self) -> List[Any]:
        """Returns all class instances"""
        return self.__all_instances

    def pretty_return(
        self,
    ) -> Tuple[Optional[int], Optional[str], Optional[str], Optional[int]]:
        """Returns all class attributes"""
        return tuple((self.counter, self.title, self.img_name, self.cluster))
