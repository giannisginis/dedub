# -*- coding: utf-8 -*-
import random
from typing import List
import itertools


class MakeDataset:
    """Create pairs out of a list of input strings
    Args:
        data_bucket = A list of Cluster class instances

    Returns:
        List[image_pairs], List[title_pairs]
    """

    def __init__(self, data_bucket: List):
        self.data_bucket = data_bucket
        self.title_pairs = []
        self.image_pairs = []

    def create_data_pairs(self, non_target_portion: float = 0.1) -> (List, List):
        """Loops over the data bucket and creates pairs
        Args:
            non_target_portion: A portion of nontarget pairs to keep
        """
        targets_title_pairs, non_targets_title_pairs = [], []
        targets_img_pairs, non_targets_img_pairs = [], []
        for data in self.data_bucket:
            for nested_data in self.data_bucket:
                if data != nested_data:
                    label = 0 if data.cluster != nested_data.cluster else 1
                    if label == 1:
                        targets_title_pairs.append(
                            (label, data.title, nested_data.title)
                        )
                        targets_img_pairs.append(
                            (label, data.img_name, nested_data.img_name)
                        )
                    else:
                        non_targets_title_pairs.append(
                            (label, data.title, nested_data.title)
                        )
                        non_targets_img_pairs.append(
                            (label, data.img_name, nested_data.img_name)
                        )

            # shuffle before selecting a portion of non-targets
            random.shuffle(non_targets_title_pairs)
            random.shuffle(non_targets_title_pairs)

            self.title_pairs.append(targets_title_pairs)
            self.title_pairs.append(
                non_targets_title_pairs[
                    : round(non_target_portion * len(non_targets_title_pairs))
                ]
            )

            self.image_pairs.append(targets_img_pairs)
            self.image_pairs.append(
                non_targets_img_pairs[
                    : round(non_target_portion * len(non_targets_img_pairs))
                ]
            )
            targets_title_pairs, non_targets_title_pairs = [], []
            targets_img_pairs, non_targets_img_pairs = [], []

        return list(itertools.chain.from_iterable(self.title_pairs)), list(
            itertools.chain.from_iterable(self.image_pairs)
        )
