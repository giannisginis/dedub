# coding: utf-8
import logging
import time
from contextlib import contextmanager
from datetime import timedelta

_logger = logging.getLogger(__loader__.name)


@contextmanager
def log_time_taken(logger=_logger, label=None):
    start = time.time()
    try:
        yield
    finally:
        end = timedelta(seconds=time.time() - start)
        logger.info(
            "%s in %s (%s seconds)",
            f"{label} finished" if label else "Finished",
            end,
            end.total_seconds(),
        )
