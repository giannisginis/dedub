# coding: utf-8
import logging.config
from typing import Dict, Any


def setup_logging(
    filename: str = None, debug_http: bool = False, debug_oauth: bool = True
) -> None:
    """
    Setup the root logger to log to stdout
    """
    config: Dict[str, Any] = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "standard": {
                "format": "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] [pid:%(process)d:%(threadName)s] %(message)s",
                "datefmt": "%d/%b/%Y %H:%M:%S",
            }
        },
        "handlers": {
            "stdout": {
                "level": "DEBUG",
                "class": "logging.StreamHandler",
                "formatter": "standard",
                "stream": "ext://sys.stdout",
            }
        },
        "loggers": {
            "urllib3": {"handlers": [], "level": "DEBUG" if debug_http else "INFO"},
            "requests_oauthlib": {
                "handlers": [],
                "level": "DEBUG" if debug_oauth else "INFO",
            },
            "geopy": {"level": "INFO"},
            "matplotlib": {"level": "INFO"},
        },
        "root": {"handlers": ["stdout"], "level": "DEBUG"},
    }
    if filename:
        config["handlers"]["file"] = {
            "level": "DEBUG",
            "class": "logging.FileHandler",
            "formatter": "standard",
            "filename": filename,
        }
        config["root"]["handlers"].append("file")
    logging.config.dictConfig(config)
