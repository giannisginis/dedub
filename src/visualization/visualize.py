import cv2
import matplotlib.pyplot as plt


def plot_two_images(first_image: str, second_image: str):
    """
    Loads two images and plots them
    """
    first_image = cv2.imread(first_image, 0)
    second_image = cv2.imread(second_image, 0)

    # initialize the figure
    fig = plt.figure("Pair #{}".format(2), figsize=(2, 2), dpi=200)

    # show first image
    ax = fig.add_subplot(1, 2, 1)
    plt.imshow(first_image, cmap=plt.cm.gray)
    plt.axis("off")
    # show the second image
    ax = fig.add_subplot(1, 2, 2)
    plt.imshow(second_image, cmap=plt.cm.gray)
    plt.axis("off")
    # show the plot
    plt.show()
