import transformers


class LoggingLevel:
    """A class to manipulate the logging level of transformers and tensorflow
    Args:
        library_to_supress: A string define which library ro supress
        level: A sting for the level of logging
    """

    def __init__(self, library_to_supress: str = "tf", level: str = "error"):
        self.library_to_supress = library_to_supress
        self.level = level

    def logging_levels_tf(self):
        """Setting logging level for tensorflow"""
        levels = {"all": "0", "info": "1", "warning": "2", "error": "3"}

        return levels.get(self.level, lambda: "Invalid")

    def logging_levels_transformers(self):
        """Setting logging level for transformers"""
        levels = {
            "debug": transformers.logging.set_verbosity_debug,
            "info": transformers.logging.set_verbosity_info,
            "warning": transformers.logging.set_verbosity_warning,
            "error": transformers.logging.set_verbosity_error,
        }

        levels.get(self.level, lambda: "Invalid")()

    def __call__(self, *args, **kwargs):
        if self.library_to_supress == "tf":
            return self.logging_levels_tf()
        elif self.library_to_supress == "transformers":
            self.logging_levels_transformers()
