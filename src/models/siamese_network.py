import tensorflow as tf
from src.features.feature_store import (
    EfficientNetFeatureExtractor,
)
from src.models.utils import LRSchedule
from src.models.semantic_similarity_networks import BertBasedUncased
from typing import Tuple, List, Dict
import numpy as np


class SiameseNetwork(tf.keras.Model):
    """Siamese Network - shared weights between two sister networks

    Args:
        image_shape: Image input size,
        output_dim: The dimension of the predicted distances,
        loss_function: Loss function,
        dropout_prob_enet: Dropoout probability,
        include_top_enet: A flag to include the top linear layers of enet,
        enet_weights: A flag to define the pretrained weights,
        is_efficient_net_trainable: A flag to use enet as pre-trained layer,
        metrics: A list of metrics,
        optimizer: A keras optimizer,
        loss_function: The loss function,
        learning_rate_params: A dictionary with the corresponding params of the lr scheduler,
        models_dir: A directory to save model checkpoints,
        use_text_encoder: A flag which indicated the use of text modalities or not,
        text_encoder: Metadata for the text encoder
    """

    def __init__(
        self,
        image_shape: Tuple[int],
        output_dim: int,
        dropout_prob_enet: int,
        include_top_enet: bool,
        enet_weights: str,
        is_efficient_net_trainable: bool,
        training_attention: bool,
        metrics: List,
        optimizer: str,
        loss_function: str,
        learning_rate_params: Dict,
        use_text_encoder: bool,
        models_dir: str = "",
        text_encoder: BertBasedUncased = None,
    ):
        super().__init__()
        self.__metrics = metrics
        self.__optimizer = optimizer
        self.__loss_function = loss_function
        self.models_dir = models_dir
        self.learning_rate_params = learning_rate_params
        self.training_attention = training_attention
        self.use_text_encoder = use_text_encoder

        # callbacks for model checkpoints
        self.checkpoint_path = (
            (
                self.models_dir
                + "/siamese_net_bert_net_cp-epoch:{epoch:02d}-metric:{val_loss:.2f}.hdf5"
            )
            if self.use_text_encoder
            else (
                self.models_dir
                + "/siamese_net_cp-epoch:{epoch:02d}-metric:{val_loss:.2f}.hdf5"
            )
        )
        self.model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
            filepath=f"{self.checkpoint_path}",
            save_weights_only=True,
            monitor="val_loss",
            mode="min",
            save_best_only=True,
        )

        self.enet_feature_extractor = EfficientNetFeatureExtractor(
            image_size=image_shape,
            output_dim=128,
            dropout_prob=dropout_prob_enet,
            include_top=include_top_enet,
            enet_weights=enet_weights,
            is_efficient_net_trainable=is_efficient_net_trainable,
        )
        if use_text_encoder:
            self.text_encoder = text_encoder
            self.text_encoder.build_model(as_encoder=True)
            self.text_encoder = self.text_encoder.model
            self.embeddings_concatenation = tf.keras.layers.concatenate
        self.distance = tf.keras.layers.Lambda(self._euclidean_distance)
        self.outputs = tf.keras.layers.Dense(output_dim, activation="sigmoid")

    def lr_schedule(self):
        return LRSchedule(
            post_warmup_learning_rate=self.learning_rate_params["init_lr"],
            warmup_steps=self.learning_rate_params["num_warmup_steps"],
        )

    @property
    def model_metrics(self):
        """Returns the models metrics"""
        kera_builtin_metrics_mapper = {
            "accuracy": "acc",
            "recall": tf.keras.metrics.Recall(),
            "precision": tf.keras.metrics.Precision(),
            "auc": tf.keras.metrics.AUC(),
        }
        metrics = []
        for str_metric in self.__metrics:
            metrics.append(kera_builtin_metrics_mapper[str_metric.lower()])
        return metrics

    @property
    def model_optimizer(self):
        """Returns the models optimizer"""
        optimizers = {
            "adam": tf.keras.optimizers.Adam,
            "adadelta": tf.keras.optimizers.Adadelta(learning_rate=self.lr_schedule),
            "sgd": tf.keras.optimizers.SGD(learning_rate=self.lr_schedule),
            "rmsprop": tf.keras.optimizers.RMSprop(learning_rate=self.lr_schedule),
        }
        return optimizers[self.__optimizer.lower()]

    @property
    def loss_function(self):
        """Returns the models' loss function"""
        return self.__loss_function

    @staticmethod
    def _euclidean_distance(vectors):
        """Calculates the euclidean Distance between two vectors
        Args:
            vectors: A tuple of vector pairs
        Returns:
            A tf Tensor with the distance
        """
        # unpack the vectors into separate lists
        feats_image_1, feats_image_2 = vectors[0], vectors[1]
        # compute the sum of squared distances between the vectors
        sum_squared = tf.reduce_sum(
            tf.square(feats_image_1 - feats_image_2), axis=1, keepdims=True
        )
        # return the euclidean distance between the vectors
        return tf.math.sqrt(tf.math.maximum(sum_squared, tf.keras.backend.epsilon()))

    def call(self, training_inputs):
        """Builds the model"""
        x1, x2 = training_inputs[0], training_inputs[1]
        x1 = self.enet_feature_extractor(x1)
        x2 = self.enet_feature_extractor(x2)
        if self.use_text_encoder:
            text_embeddings = self.text_encoder(training_inputs[2])
            x1 = self.embeddings_concatenation([x1, text_embeddings])
            x2 = self.embeddings_concatenation([x2, text_embeddings])
        x = self.distance([x1, x2])
        outputs = self.outputs(x)

        return outputs

    @staticmethod
    def loss(margin=1):
        """Provides 'constrastive_loss' an enclosing scope with variable 'margin'.

        Arguments:
            margin: Integer, defines the baseline for distance for which pairs
                    should be classified as dissimilar. - (default is 1).

        Returns:
            'constrastive_loss' function with data ('margin') attached.
        """

        # Contrastive loss = mean( (1-true_value) * square(prediction) +
        #                         true_value * square( max(margin-prediction, 0) ))
        def contrastive_loss(y_true, y_pred):
            """Calculates the constrastive loss.

            Arguments:
                y_true: List of labels, each label is of type float32.
                y_pred: List of predictions of same length as of y_true,
                        each label is of type float32.

            Returns:
                A tensor containing constrastive loss as floating point value.
            """

            square_pred = tf.math.square(y_pred)
            margin_square = tf.math.square(tf.math.maximum(margin - (y_pred), 0))
            return tf.math.reduce_mean(
                (1 - y_true) * square_pred + (y_true) * margin_square
            )

        return contrastive_loss


class CustomModelCheckPoint(tf.keras.callbacks.Callback):
    """Stop training when the loss is at its min, i.e. the loss stops decreasing.

    Arguments:
        filepath: filepath of the trained model.
    """

    def __init__(self, filepath):
        super(CustomModelCheckPoint, self).__init__()
        self.filepath = filepath
        # best_weights to store the weights at which the minimum loss occurs.
        self.best_loss = None
        self.current_epoch = None

    def on_train_begin(self, logs=None):
        """Initialize the best as infinity."""
        self.best_loss = np.Inf

    def on_epoch_end(self, epoch, logs=None):
        """Handles the saving of the checkpoint
        Args:
            epoch: The current epoch
            logs: logs
        """
        current_loss = logs.get("loss")
        if np.less(current_loss, self.best_loss):
            self.best_loss = current_loss
            self.model.save(
                f"self.filepath/siamese_net_cp-epoch:{epoch:02d}-metric:{current_loss:.2f}.tf",
                overwrite=True,
                include_optimizer=True,
                save_format="tf",
            )
